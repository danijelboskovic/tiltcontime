package hr.keyboards.sl.tiltcontinputmethod;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.inputmethodservice.InputMethodService;
import android.inputmethodservice.KeyboardView;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import java.util.Locale;
import java.util.Timer;

/**
 * Created by Sandi on 16.2.2016..
 */
public class TiltContIME extends InputMethodService
        implements KeyboardView.OnKeyboardActionListener, SensorEventListener {

    LayoutInflater inflater;

    //kemijanje s pozicijama
    private int x=4;
    private int y=0;
    private boolean shiftOn=false;
    private boolean altOn=false;
    public Button bKey[] = new Button[33];
    public Button bKeyFoc = bKey[0];
    private boolean firstMove = false;

    //senzorika
    public Sensor accelerometer;
    public SensorManager sm;
    public float sX=0, sY=0,sZ=0;
    public int safeZone=1;
    public int nothingMoved=0;
    public double sXspeed1low = 2;
    public double sXspeed1high = 4;
    public double sXspeed2low = 4;
    public double sXspeed2high = 6;
    public double sXspeed3low = 6;
    public int interval1speed = 700;
    public int interval2speed = 500;
    public int interval3speed = 300;
    public int intervalDwell = 1000;
    public boolean canDelete = false;
    public int dwellDelete = 500;
    public boolean soundOn = true;
    public boolean vibrationOn = true;


    //ništa bez zvuka
    public SoundPool sp = null;
    public int iTmp = 0;

    //šablona
    public String[] keyListMain =  {"q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","Del","Sft","z","x","c","v","b","n","m",".","Alt","?",",","Space"};
    String[] keyListShift = {"Q","W","E","R","T","Y","U","I","O","P","A","S","D","F","G","H","J","K","L","Del","Sft","Z","X","C","V","B","N","M",".","Alt","?",",","Space"};
    String[] keyListAlt =   {"1","2","3","4","5","6","7","8","9","0","@","#","$","%","*","-","+","(",")","!","Sft","!","\"","'",":",";","/","?",".","Alt","?",",","Space"};

    //hidden
    public boolean paused = true;

    //timer
    public final Handler handler = new Handler();
    public Timer timer = new Timer();
    //jedan ciklus (ms)
    int interval=500;

    View rootview;

    @Override
    public void onCreate() {
        super.onCreate();

        // no dim support
        getWindow().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        inflater = getLayoutInflater();
        rootview = inflater.inflate(R.layout.main_layout, null);

        //senzorika
        sm = (SensorManager)getSystemService(SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        loadSettings();
        //zvuk
        //mp = MediaPlayer.create(this, R.raw.click);
        sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        iTmp = sp.load(this, R.raw.click, 1);



        //incijalizacija onclick listenera tipki tipkovnice (da se utvrde osnovne funkcionalnosti)
        bKey[0] = (Button) rootview.findViewById(R.id.bKey0);
        bKey[1] = (Button) rootview.findViewById(R.id.bKey1);
        bKey[2] = (Button) rootview.findViewById(R.id.bKey2);
        bKey[3] = (Button) rootview.findViewById(R.id.bKey3);
        bKey[4] = (Button) rootview.findViewById(R.id.bKey4);
        bKey[5] = (Button) rootview.findViewById(R.id.bKey5);
        bKey[6] = (Button) rootview.findViewById(R.id.bKey6);
        bKey[7] = (Button) rootview.findViewById(R.id.bKey7);
        bKey[8] = (Button) rootview.findViewById(R.id.bKey8);
        bKey[9] = (Button) rootview.findViewById(R.id.bKey9);
        bKey[10] = (Button) rootview.findViewById(R.id.bKey10);
        bKey[11] = (Button) rootview.findViewById(R.id.bKey11);
        bKey[12] = (Button) rootview.findViewById(R.id.bKey12);
        bKey[13] = (Button) rootview.findViewById(R.id.bKey13);
        bKey[14] = (Button) rootview.findViewById(R.id.bKey14);
        bKey[15] = (Button) rootview.findViewById(R.id.bKey15);
        bKey[16] = (Button) rootview.findViewById(R.id.bKey16);
        bKey[17] = (Button) rootview.findViewById(R.id.bKey17);
        bKey[18] = (Button) rootview.findViewById(R.id.bKey18);
        bKey[19] = (Button) rootview.findViewById(R.id.bKey19);
        bKey[20] = (Button) rootview.findViewById(R.id.bKey20);
        bKey[21] = (Button) rootview.findViewById(R.id.bKey21);
        bKey[22] = (Button) rootview.findViewById(R.id.bKey22);
        bKey[23] = (Button) rootview.findViewById(R.id.bKey23);
        bKey[24] = (Button) rootview.findViewById(R.id.bKey24);
        bKey[25] = (Button) rootview.findViewById(R.id.bKey25);
        bKey[26] = (Button) rootview.findViewById(R.id.bKey26);
        bKey[27] = (Button) rootview.findViewById(R.id.bKey27);
        bKey[28] = (Button) rootview.findViewById(R.id.bKey28);
        bKey[29] = (Button) rootview.findViewById(R.id.bKey29);
        bKey[30] = (Button) rootview.findViewById(R.id.bKey30);
        bKey[31] = (Button) rootview.findViewById(R.id.bKey31);
        bKey[32] = (Button) rootview.findViewById(R.id.bKey32);

        for(int i = 0; i<33; i++){
            bKey[i].setTransformationMethod(null);
        }

        refreshPosition();

        for (int i=0;i<33;i++){
            bKey[i].setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if(!canWrite()) return;
                    Button b = (Button)v;
                    String str= b.getText().toString();
                    System.out.println("Button clicked: " + str);
                    if(str.equals("Space") || str.equals("Del") || str.equals("Sft") || str.equals("Alt") ){
                        if(str.equals("Space")){
                            canDelete = false;
                            addLetter(" ");
                        }
                        if(str.equals("Del")){
                            canDelete = true;
                            deleteLetter();
                        }
                        if(str.equals("Sft")){
                            canDelete = false;
                            toggleShift();
                        }
                        if(str.equals("Alt")){
                            canDelete = false;
                            toggleAlt();
                        }
                    }else{
                        canDelete = false;
                        addLetter(str);
                    }
                }
            });
        }
        timerSet();
        System.out.println("outTimerSet + " + interval);
        /*TimerTask testing = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        //Toast.makeText(MainActivity.this, "test", Toast.LENGTH_SHORT).show();
                        doTick();
                    }
                });
            }
        };
        timer.schedule(testing, 700, interval);*/
        setInputView(rootview);
        updateInputViewShown();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        Display display = ((WindowManager) getSystemService(WINDOW_SERVICE))
                .getDefaultDisplay();

        int orientation = display.getRotation();

        if (orientation == Surface.ROTATION_90
                || orientation == Surface.ROTATION_270) {
            onWindowHidden();
            rootview.setVisibility(View.INVISIBLE);
            Toast.makeText(TiltContIME.this, "Keyboard can only work in Portrait mode, please turn off screen rotation!",
                    Toast.LENGTH_LONG).show();
        } else if (orientation == Surface.ROTATION_0 || orientation == Surface.ROTATION_180){
            System.out.println("Port");
            rootview.setVisibility(View.VISIBLE);
            onWindowShown();
        }
    }

    public void loadSettings(){
        System.out.println("loadSettings()");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        sXspeed1low = Float.parseFloat(prefs.getString(getString(R.string.pref_sXspeed1low),getString(R.string.pref_sXspeed1low_default)));
        System.out.println("sXspeed1low " + sXspeed1low);
        if (sXspeed1low < 10 || sXspeed1low >90){
            sXspeed1low = 2;
        } else {
            sXspeed1low = sXspeed1low / 9;
        }
        System.out.println("sXspeed1low " + sXspeed1low);
        sXspeed2low = Float.parseFloat(prefs.getString(getString(R.string.pref_sXspeed2low),getString(R.string.pref_sXspeed2low_default)));
        if (sXspeed2low < 10 || sXspeed2low >90){
            sXspeed2low = 4;
        } else {
            sXspeed2low = sXspeed2low / 9;
        }
        sXspeed1high = sXspeed2low;
        System.out.println("sXspeed1high " + sXspeed1high);
        System.out.println("sXspeed2low " + sXspeed2low);
        sXspeed3low = Float.parseFloat(prefs.getString(getString(R.string.pref_sXspeed3low),getString(R.string.pref_sXspeed3low_default)));
        System.out.println("sXspeed2high " + sXspeed2high);
        System.out.println("sXspeed3low " + sXspeed3low);
        if (sXspeed3low < 10 || sXspeed3low >90){
            sXspeed3low = 6;
        } else {
            sXspeed3low = sXspeed3low / 9;
        }
        sXspeed2high = sXspeed3low;
        System.out.println("sXspeed2high " + sXspeed2high);
        System.out.println("sXspeed3low " + sXspeed3low);
        interval1speed = Integer.parseInt(prefs.getString(getString(R.string.pref_interval1Speed),getString(R.string.pref_interval1Speed_default)));
        System.out.println("interval1speed = " + interval1speed);
        interval2speed = Integer.parseInt(prefs.getString(getString(R.string.pref_interval2Speed),getString(R.string.pref_interval2Speed_default)));
        System.out.println("interval2speed = " + interval2speed);
        interval3speed = Integer.parseInt(prefs.getString(getString(R.string.pref_interval3Speed),getString(R.string.pref_interval3Speed_default)));
        System.out.println("interval3speed = " + interval3speed);
        intervalDwell = Integer.parseInt(prefs.getString(getString(R.string.pref_dwellInterval),getString(R.string.pref_dwellInterval_default)));
        System.out.println("interval Dwell = " + intervalDwell);
        dwellDelete = Integer.parseInt(prefs.getString(getString(R.string.pref_dwell_delete), getString(R.string.pref_dwellDelete_default)));
        if(prefs.getBoolean(getString(R.string.pref_soundOn), true)){
            soundOn = true;
        } else {
            soundOn = false;
        }
        System.out.println("Sound = " + soundOn);
        if(prefs.getBoolean(getString(R.string.pref_vibrationOn), true)){
            vibrationOn = true;
        } else {
            vibrationOn = false;
        }
        System.out.println("Vibration = " + vibrationOn);

    }

    public void timerSet(){

        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                doTick();
                handler.postDelayed(this, interval);
            }
        };
        handler.postDelayed(runnable, interval);
        System.out.println("timer + " + interval);
    }


    public int deleteLetter()
    {
        getCurrentInputConnection().deleteSurroundingText(1, 0);
        return 0;
    }

    public int toggleShift()
    {
        shiftOn=!shiftOn;
        if (shiftOn && altOn){
            shiftOn=!shiftOn;
            return 0;
        }
        int i=0;
        if(shiftOn){
            for (i=0;i<33;i++){
                bKey[i].setText(keyListShift[i]);
            }
        }else{
            for (i=0;i<33;i++){
                bKey[i].setText(keyListMain[i]);
            }
        }
        return 0;
    }

    public int toggleAlt()
    {
        altOn=!altOn;
        int i=0;
        if(altOn){
            for (i = 0; i < 33;i++){
                bKey[i].setText(keyListAlt[i]);
            }
        }else{
            for (i=0;i<33;i++){
                bKey[i].setText(keyListMain[i]);
            }
        }
        return 0;
    }

    public int addLetter(String s)
    {
        System.out.println("addletter!");
        if(!shiftOn){
            getCurrentInputConnection().commitText(String.valueOf(s), 1);
        }else{
            getCurrentInputConnection().commitText(String.valueOf(s.toUpperCase(Locale.ENGLISH)), 1);
        }
        return 0;
    }

    public void submitAction()
    {
        if(!canWrite()) return;
        bKeyFoc.performClick();
        if(canPlaySound()) playSound();
        if(canVibrate()) ((Vibrator)getSystemService(VIBRATOR_SERVICE)).vibrate(800);
        System.out.println("TU SAM - Submit");
        //editText.setSelection(editText.getText().length());
    }

    public void playSound() {
        sp.play(iTmp, 1, 1, 0, 0, 1);
    }

    public void refreshPosition()
    {
        System.out.println("x: " + x + "  y: " + y);
        switch (y) {
            case 0:
                System.out.println(x);
                bKeyFoc = bKey[x];
                System.out.println("focused button: "+ x);
                break;
            case 1:
                System.out.println(x);
                bKeyFoc = bKey[x+10];
                System.out.println("focused button:" + x + 10);
			/*if(x==9){
				bKeyFoc = bKey[18];
				System.out.println("focused button: "+ 18);
			} else {
				bKeyFoc = bKey[x+10];
				System.out.println("focused button: "+ x+ 10);
			}*/
                break;
            case 2:
			/*2
			if(x>=0 && x<=8){
				bKeyFoc = bKey[x+10+9];
				System.out.println("focused button: " + x+10+9);
			}
			if(x==9){
				bKeyFoc = bKey[27];
			}*/

                System.out.println(x);
                bKeyFoc = bKey[x+10+10];
                System.out.println("focused button: " + x + 20);

                break;
            case 3:
                System.out.println(x);
			/*if(x==0){
				bKeyFoc = bKey[28];
				System.out.println("focused button: " + 28);
			}
			if(x==1){
				bKeyFoc = bKey[29];
				System.out.println("focused button: " + 29);
			}
			if(x==2){
				bKeyFoc = bKey[30];
				System.out.println("focused button: " + 30);
			}
			if(x==3){
				bKeyFoc = bKey[31];
				System.out.println("focused button: " + 31);
			}
			if(x>3){
				bKeyFoc = bKey[32];
				System.out.println("focused button: " + 32);
			}*/
                if(x == 0){
                    bKeyFoc = bKey[30];
                    System.out.println("focused button:" + 30);
                }
                if(x == 1){
                    bKeyFoc = bKey[31];
                    System.out.println("focused button:" + 31);
                }
                if(x > 1){
                    bKeyFoc = bKey[32];
                    System.out.println("focused button:" + 32);
                }



			/*
			if(x>=0 && x<=2) {
				bKeyFoc = bKey[30];
				System.out.println("focused button: " + 30);
			}
			if(x>=3 && x<=6) {
				bKeyFoc = bKey[31];
				System.out.println("focused button: " + 31);
				}
			if(x>=7 && x<=9) {
				bKeyFoc = bKey[32];
				System.out.println("focused button: " + 32);
			}
			*/
                //System.out.println("focused button: " + x+ 30);
                break;
            default:
                System.out.println(x);
                bKeyFoc= bKey[0];
                break;
        }

        colorReset(bKey);
        bKeyFoc.setBackgroundColor(Color.BLACK);

    }


    public int colorReset(Button[] bKey)
    {
        int i;
        for (i=0;i<33;i++){
            bKey[i].setBackgroundColor(Color.GRAY);
        }
        if(shiftOn)	bKey[20].setBackgroundColor(Color.CYAN);
        if(altOn)	bKey[29].setBackgroundColor(Color.CYAN);

        return 0;
    }

    public boolean canPlaySound() { return  soundOn; }

    public boolean canVibrate() { return vibrationOn; }

    public boolean canDeleteCont() { return canDelete; }

    public boolean canWrite(){
        return firstMove;
    }

    public int doTick(){
        if (!paused ) {
            /*acceleration.setText("X: "+ sX +
                    "\nY: " + sY +
                    "\nZ: " + sZ);
*/


            boolean moveNoted = false;

            if (sX > sXspeed1low && sX < sXspeed1high) {

                interval = interval1speed;
                pushLeft();
                System.out.println("pushleft1speed() ");
                System.out.println("Interval " + interval);
                System.out.println("interval1speed " + interval1speed);
                System.out.println("sx1l" + sXspeed1low);
                System.out.println("sx" + sX);
                moveNoted = true;
                canDelete = false;
            }
            if (sX > sXspeed2low && sX < sXspeed2high) {

                interval = interval2speed;
                pushLeft();
                System.out.println("sx2l" + sXspeed2low);
                System.out.println("sx" + sX);
                moveNoted = true;
                canDelete = false;
            }
            if (sX > sXspeed3low) {


                interval = interval3speed;
                pushLeft();
                System.out.println("sx3l" + sXspeed3low);
                System.out.println("sx" + sX);
                moveNoted = true;
                canDelete = false;
            }
            if (sX < -sXspeed1low && sX > -sXspeed1high) {

                interval = interval1speed;
                pushRight();

                moveNoted = true;
                canDelete = false;
            }
            if (sX < -sXspeed2low && sX > -sXspeed2high) {

                interval = interval2speed;
                pushRight();

                moveNoted = true;
                canDelete = false;
            }
            if (sX < -sXspeed3low) {

                interval = interval3speed;
                pushRight();

                moveNoted = true;
                canDelete = false;
            }
            if (sY > sXspeed1low && sY < sXspeed1high) {

                interval = interval1speed;
                pushDown();

                moveNoted = true;
                canDelete = false;
            }
            if (sY > sXspeed2low && sY < sXspeed2high) {

                interval = interval2speed;
                pushDown();

                moveNoted = true;
                canDelete = false;
            }
            if (sY > sXspeed3low) {


                interval = interval3speed;
                pushDown();

                moveNoted = true;
                canDelete = false;
            }
            if (sY < -sXspeed1low && sY > -sXspeed1high) {

                interval = interval1speed;
                pushUp();

                moveNoted = true;
                canDelete = false;
            }
            if (sY < -sXspeed2low && sY > -sXspeed2high) {

                interval = interval2speed;
                pushUp();

                moveNoted = true;
                canDelete = false;
            }
            if (sY < -sXspeed3low) {

                interval = interval3speed;
                pushUp();

                moveNoted = true;
                canDelete = false;
            }
            if ((sX < safeZone || sX > -safeZone) || (sY < safeZone || sY > -safeZone)) {
                if(canDeleteCont()){
                    interval = dwellDelete;
                    if (!moveNoted) {
                        nothingMoved++;
                        //interval = 300;
                    } else {
                        nothingMoved = 0;
                    }
                    if (nothingMoved >= 2) {

                        submitAction();
                        //interval = interval1speed;
                        nothingMoved = 0;
                        firstMove = true;
                    }
                }else {
                    if (!moveNoted) {
                        nothingMoved++;
                    } else {
                        nothingMoved = 0;
                    }
                    if (nothingMoved >= 2) {
                        interval = intervalDwell;
                        submitAction();
                        interval = interval1speed;
                        nothingMoved = 0;
                        firstMove = false;
                    }
                }

                refreshPosition();
                System.out.println("Refresh position");
            }
        }
        return 0;
    }


    public void pushUp()
    {
        System.out.println("pushUp");
        firstMove = true;
        if(canPlaySound()) playSound();
        y--;
        if (y<0){
            y=3;
        }
    }

    public void pushDown()
    {
        System.out.println("pushDown");
        firstMove = true;
        if(canPlaySound()) playSound();
        y++;
        if(y>3){
            y=0;
        }
    }

    public void pushLeft()
    {
        System.out.println("pushLeft");
        firstMove = true;
        if(canPlaySound()) playSound();
        switch (y) {

            case 0:
                x--;
                if(x<0) x=9;
                break;
            case 1:
                x--;
                if(x<0) x=9;
                break;
            case 2:
                x--;
                if(x<0) x=9;
                break;
            case 3:
                x--;
                if(x<0) x=2;
                break;
            default:

                break;
        }
    }

    public void pushRight()
    {
        System.out.println("pushRight");
        firstMove = true;
        if(canPlaySound()) playSound();
        switch (y) {
            case 0:
                x++;
                if(x>9) x=0;
                break;
            case 1:
                x++;
                if(x>9) x=0;
                break;
            case 2:
                x++;
                if(x>9) x=0;
                break;
            case 3:
                x++;
                if(x>2) x=0;
                break;
            default:
                break;
        }
    }



    @Override
    public View onCreateInputView() {
        return null;
    }

    @Override
    public void onFinishInputView(boolean finishingInput) {
        super.onFinishInputView(finishingInput);
    }

    @Override
    public void onWindowShown() {
        super.onWindowShown();
loadSettings();
        paused = false;
        System.out.println("Paused = false");
    }

    @Override
    public void onWindowHidden() {
        super.onWindowHidden();
        soundOn = false;
        vibrationOn = false;
        paused = true;
    }


    @Override
    public void onKey(int primaryCode, int[] keyCodes) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeRight() {

    }

    @Override
    public void swipeLeft() {

    }

    @Override
    public void swipeDown() {

    }

    @Override
    public void swipeUp() {
    }

    @Override
    public void onPress(int primaryCode) {
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    @Override
    public void onSensorChanged(SensorEvent event) {
        sX=event.values[0];
        sY=event.values[1];
        sZ=event.values[2];
    }

}
